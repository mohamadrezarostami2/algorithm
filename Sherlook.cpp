#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    long long N,Ai,totalsum,sum,i,temp;
    int T;
    int f=0;
    cin >> T;
    for(int j=0;j<T;j++){
        cin >> N;
        totalsum=0;
        vector<long long> num;
        if(N == 1){
            cin >> temp;
            cout<<"YES\n";
            continue;
        }
        for(i=0;i<N;i++){
            cin >> temp;
            totalsum+=temp;
            num.push_back(temp);
        }
        f=0;
        sum=num[0];
        for(i=1;i<N;i++){
            temp=totalsum-num[i]-2*sum;
            if(!temp){
                cout<<"YES\n";
                f=1;
                break;
            }
            sum+=num[i];
        }
        if(i == N ) cout<<"NO\n";
    }
    return 0;
}