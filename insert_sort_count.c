// C program to insert a node in AVL tree
#include <stdio.h>
#include <stdlib.h>
 
// An AVL tree node
struct Node
{
    int key;
    long long index;
    long long indexleft;
    struct Node *left;
    struct Node *right;
    int height;
};

typedef struct Node Node;

// A utility function to get maximum of two integers
int max(int a, int b);
 
// A utility function to get height of the tree
int height(struct Node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}
 
// A utility function to get maximum of two integers
int max(int a, int b)
{
    return (a > b)? a : b;
}
 
/* Helper function that allocates a new node with the given key and
    NULL left and right pointers. */
struct Node* newNode(int key)
{
    struct Node* node = (struct Node*)
                        malloc(sizeof(struct Node));
    node->key   = key;
    node->index = 1;
    node->indexleft =0;
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;  // new node is initially added at leaf
    return(node);
}
 
// A utility function to right rotate subtree rooted with y
// See the diagram given above.
struct Node *rightRotate(struct Node *y)
{
    struct Node *x = y->left;
    if(x == NULL){
        return y;
    }
    struct Node *T2 = x->right;
 
    // Perform rotation

    x->right = y;
    y->left = T2;
 
    // Update heights
    //x->index=y->index;
    if(T2){
        y->indexleft=(T2->index)+(T2->indexleft);
    }
    else
        y->indexleft=0;

    x->index=(y->index)+(y->indexleft)+1;

    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    // Return new root
    return x;
}
 
// A utility function to left rotate subtree rooted with x
// See the diagram given above.
struct Node *leftRotate(struct Node *x)
{
    struct Node *y = x->right;
    struct Node *T2 = y->left;
    // Perform rotation
    y->left = x;
    x->right = T2;
 	
 	//x->index-=y->index;
 	if(T2){
    	x->index=(T2->index)+1+(T2->indexleft);
	}
    else{
        x->index=1;   
    }
    y->indexleft=(x->index)+(x->indexleft);
    //  Update heights
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    // Return new root
    return y;
}
 
// Get Balance factor of node N
int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    //printf("%d\n",N->left );
    return height(N->left) - height(N->right);
}
 
// Recursive function to insert key in subtree rooted
// with node and returns new root of subtree.
struct Node* insert(struct Node* node, int key,long long* swep)
{
    /* 1.  Perform the normal BST insertion */
    if (node == NULL)
        return(newNode(key));
 
    if (key < node->key){
    	(*swep)+=node->index;
        node->indexleft+=1;
        node->left  = insert(node->left, key,swep);
    }
    else if (key >= node->key){
    	node->index+=1;
    	node->right = insert(node->right, key,swep);
    }
/*    else // Equal keys are not allowed in BST
        return node;*/
 
    /* 2. Update height of this ancestor node */
    node->height = 1 + max(height(node->left),
                           height(node->right));
 
    /* 3. Get the balance factor of this ancestor
          node to check whether this node became
          unbalanced */
    int balance = getBalance(node);
 
    // If this node becomes unbalanced, then
    // there are 4 cases
 
    // Left Left Case
     if (balance > 1 && key < node->left->key)
        return rightRotate(node);
 
    // Right Right Case
    if (balance < -1 && key >= node->right->key)
        return leftRotate(node);
 
    // Left Right Case
    if (balance > 1 && key >= node->left->key)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
 
    // Right Left Case
    if (balance < -1 && key < node->right->key)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    /* return the (unchanged) node pointer */
    return node;
}
 
// A utility function to print preorder traversal
// of the tree.
// The function also prints height of every node
void preOrder(struct Node *root)
{
    if(root != NULL)
    {
        printf("%d ", root->key);
        preOrder(root->left);
        preOrder(root->right);
    }
}



int main(){
	int* list=NULL;
	int* sortlist=NULL;
	int T=0;
	int N=0;
	int n=0;
	int t=0;
	long long swep=0;
	int count=0;
	int i=0;
	int num,num1;
	scanf("%d",&T);
	Node* pointer=NULL;
	for(t=0;t<T;t++){
		scanf("%d",&N);
		Node* root=NULL;
		for(n=0;n<N;n++){
			scanf("%d",&num);
			root=insert(root,num,&swep);
		}
		printf("%llu\n",swep);
		swep=0;
	}
}